# Programar en .BAT

Los ficheros *.bat* es texto con comandos para el SO Windows, para ejecutar tareas y procesos.

## Ejecutar .BAT

En una carpeta que podemos o no añadir al *PATH* con la opción `c:\>set path=%path%; c:\carpetaFicherosBat`.

Para que sea permante una ruta al *Path* se debe realizar a través de las variables de entorno.

## Comentarios en .BAT

Para crear comentarios, hay dos maneras:
1. **REM**. Al escribir *REM* al principio de la línea indicamos que es un comentario.
2. **::** La otra forma es escribiendo dos veces los puntos dobles al inicio de la línea.

Ejemplo 1: `REM esto es un comentario`. <br>
Ejemplo 2: `:: esto también es un comentario`.

## ECHO en .BAT

Al ejecutar un fichero *.bat* en la consola (cmd) veremos cada una de las instrucciones ejecutadas. Para los casos en los que no nos interese ver la ejecución línea por línea se puede poner al inicio del código la instrucción:
* `@ECHO OFF`.   
* `ECHO ON` (muestra las instrucciones, está por defecto).   
* `ECHO OFF` (no muestra los comandos que están después).   
* `ECHO [mensaje]` (muestra mensaje por pantalla. Se puede redireccionar con **>** para sobrescribir el contenido ó **>>** añade a continuación de lo que ya estaba).   
* `ECHO` (crea una línea en blanco).   

## Pause en .BAT

El comando `pause` pausará el programa hasta que pulsemos una tecla.
El comando `pause>NUL` pausará el programa, pero no mostrará ningún mensaje de texto.

## Parámetros en .BAT

Se pueden introducir parámetros después del nombre del *fichero.bat*.
- **%n**, dónde *n* es un valor comprendido entre 1 y 9.

> @ECHO OFF   
> ECHO Hola mi nombre es %1   
> ECHO Y mi apellido es %2.   

El programa se ejecuta *parametros.bat Luis Rojas*.

## Condicional IF en .BAT

La instrucción **IF** permite crear una condición y seleccionar de entre dos líneas de código. La sintaxis es:

+ IF [NOT] condition command
+ IF [NOT] ERRORLEVEL

Dónde `[NOT]` es opcional, ejecutando el comando si *no* se cumple la condición. En `condtion` se añade la condición que permite derivar la ejecución del programa; con **EXIST nombreArchivo** comprueba si existe el fichero; con **stringA==stringB** compara si dos cadenas de texto son iguales diferenciando entre mayúsculas y minúsculas. Por último en `command` están los comandos a ejecutar en caso de que la condición se cumplió. Si queremos ejecutar varios comandos, entre cada uno incluiremos el símbolo de **&** (corresponde con el Y lógico o la puerta lógica AND).

| Operador  | Explicación       |
|   :----:  | :---------------- |
| EQU       | igual a           |
| NEQ       | no es igual a     |
| LSS       | menor que         |
| LEQ       | menor o igual a   |
| GTR       | mayor a           |
| GEQ       | mayor o igual a   |

Con `ERRORLEVEL` cada orden genera un código de salida que indica si se ejecutó sin problemas o si hubo algún error durante el proceso. El código 0 significa que no hubo ningún error.

```
@ECHO OFF
REM Poner una extensión como parámetro
REM Comprueba si existe al menos un archivo
REM Con la extensión especificada
REM En el directorio actual
::------------------------------------------
IF EXIST *.%1 ECHO Hay al menos un archivo con la extensión .%1
IF NO EXIST *.%1 ECHO No hay ningún archivo con la extensión .%1.
```

El script anterior escrito con **ELSE**.

```
IF EXIST *.%1 (ECHO Hay al menos un archivo con la extensión .%1
) ELSE (
ECHO No hay ningún archivo con la extensión .%1.)
```

## GOTO en .BAT

Permite realizar saltos tanto hacia delante como hacia detrás en el normal proceso secuencial de ejecución. La sintaxis es:

> GOTO etiqueta   
> ... (líneas de código aquí)   
> :etiqueta   

Un ejemplo de un script con el uso de **GOTO nombreEtiqueta**.

```
@ECHO OFF
REM Uso del goto 
::-------------------------------------------------------

IF EXIST *.%1 ECHO Hay al menos un archivo .%1 & GOTO fin
ECHO No hay ningún archivo con esa extensión.

REM poner otras líneas de código aquí

:fin
```

## CHOICE en .BAT

Nos permite una opción entre las que se nos aparecen en pantalla. La sintaxis es: **CHOICE [/C teclas][/N][/S][T nn][/D predeterminado][texto]**. Dónde `[/C teclas]` indicamos las teclas con las que se hará la selección (por defecto es S o N). Con `[/N]` permite que no se muestren las opciones por pantalla. Con `[/S]` permite distinguir entre minúsculas y mayúsculas. Con `[/T nn]` sustituye las *n* por un número indica el tiempo que esperará antes de escoger la opción predeterminada. Con `[/D predeterminado]` indica la opción por defecto que se escogerá tras un determinado tiempo, esta opción debe estar incluida en el */C*. Por último, la opción `[Texto]`, muestra el texto que le aparecerá al usuario a la hora de elegir opciones.

Para ejecutar la opción, debemos usar `IF ERRORLEVEL` y el código del *errorlevel* que irá desde la última opción hasta la primera.

```
@ECHO OFF
:inicio
ECHO Qué quieres hacer?
CHOICE /C CDS /T 20 /D S /M "Abrir Calculadora, mostrar Directorios, Salir (20 segundos para S)"

IF ERRORLEVEL 3 GOTO salir
IF ERRORLEVEL 2 GOTO directorios
IF ERRORLEVEL 1 GOTO calculadora
:calculadora
    START calc.exe & GOTO fin

:directorios
    DIR & GOTO fin

:salir
    EXIT

:fin

ECHO Desea hacer algo más?
CHOICE /T 10 /D N

IF ERRORLEVEL 2 GOTO salir
IF ERRORLEVEL 1 GOTO inicio
```

## SET en .BAT

El comando **SET** nos permite crear variables de entorno. La sintaxis es: `SET [parametro][nombre=[string]]`. Dónde `[parametro]` puede ser **/a** para valores numéricos o **/p** de tipo *string* será el texto que aparecerá antes de introducir el valor de la variable, es decir, **/p** lee los valores del teclado. Por último, `[nombre=[string]]` establece un nombre de la variables y le atribuye un valor.

El siguiente *script* pide al usuario que escriba un nombre, comprueba si la variabla está vacía e imprime en pantalla el texto guardado en la variable.

```
@ECHO OFF

:errorname
SET /p name="Escribe un nombre: "
IF "%name%"=="" GOTO errorname

ECHO El nombre que has escrito es %name%
```

## El bucle FOR en .BAT

Se utiliza para realizar bucles definidos hasta que se cumpla una determinada condición. La sintaxis es: `FOR [parametros] %%variable IN (conjunto) DO comando`. Dónde **/F** da la opción de recorrer las líneas de un fichero de texto y almacenarlas en la variable contenedora **%%**. Se puede usar con *eol*, *skip*, *delims* o *tokens*.

- **eol=c**. Indica que carácter hace que la línea sea un comentario, este carácter tiene que iniciar la línea.
- **skip=n**. Especificando un número indica cuantas líneas del comienzo del archivo se salta.
- **delims=c**. Especificando un carácter o unos pocos, indicamos que caracteres dividirán las líneas en bloques para usar junto con el parámetro *tokens*.
- **tokens=x,y,m-n**. Los valores x, y, z, asignan el número correspondiente de cada línea al parámetro *%%i, %%j, %%k* o el que se asigne como inicio. El *m-n* indica lo mismo solo que en un intervalo desde *m* hasta *n*.
- **/L**. Permite especificar un rango en el conjunto *(inicio, salto, final)*. Ejemplo, el conjunto *(2,3,50) irá desde el 2 al 50 dando saltos de 3 en 3.
- **/R**. Permite especificar una ruta en la que se ejecutará el comando.

Finalmente **%%variable** almacena cada uno de los valores del conjunto. Y **(conjunto)** puede ser una serie de archivos sobre los que se aplicará un comando, una serie de números o un rango si utilizamos el */L*.

El script muestra los números del 1 al 100, con saltos de dos en dos. Después muestra todos los *.exe*, los *.bat* y los *.com* del directorio actual y por último mostrará por pantalla todo el contenido de todos los *.txt*.

```
@ECHO OFF

FOR /L %%i IN (1,2,100) DO ECHO %%i
FOR %%i IN (exe com bat) DO DIR *.%%i
FOR %%i IN (txt) DO (TYPE *.%%i)
```

## CALL o START en .BAT

Ambas instrucciones tienen la misma funcionalidad, y es el de ejecutar un programa externo al fichero .bat que realiza la llamada. La sintaxis es: **CALL nombreDelPrograma**.

**START** tiene la misma sintaxis, pero tiene muchos más atributos, que lo hacen más versátil. Para ver todos los atributos, escribir **start /?** en consola del cmd.

Estos comandos permiten ejecutar un programa y posteriormente una vez se esté ejecutando, vuelve al *.bat* y continúa en la línea de ejecución. Si no se pusiera ninguno de estos dos comandos y se pusiera directamente el nombre del archivo la línea de ejecución finalizaría con el programa, ya que **CALL** y **START** hacen que continúe la ejecución.

**CALL** se ejecuta en la misma ventana en la que se usa y también se puede utilizar para llamar a funciones. Para que **START** se ejecute en la misma ventana que en la que se ejecuta, se debe utilizar con el parámetro **/B**.

## Funciones en .BAT

Las *funciones* permiten reutilizar código. Las *funciones* deben estar al inicio del programa.

```
@ECHO OFF&CALL:inicio&GOTO:EOF

:funcion1
    ::aquí el código de la función 1
GOTO:EOF

:funcion2
    ::aquí el código de la función 2
GOTO:EOF

:inicio
    ::Programa principal
    CALL:funcion2
    CALL:funcion1
```

El código anterior, una vez que se lee la primera línea saltará directamente a la etiqueta `:inicio` y ejecutará las instrucciones que estén escritas. En este caso se ejecutará primero la *funcion2* y después la *funcion1*. El fin de la función se indica con `GOTO:EOF`.

A las *funciones* también se les pueden pasar valores que tomarán forma de parámetros.

```
@ECHO OFF&CALL:inicioCalculadora&GOTO:EOF
:suma
    SET /A solucion=%1 + %2
    ECHO %solucion%
GOTO:EOF

:resta
    SET /A solucion=%1 - %2
    ECHO %solucion%
GOTO:EOF

:inicioCalculadora
    SET /P numeros="Intro dos números separados por un espacio: "
    CHOICE /C SR /M "Escoge Suma o Resta"
    IF ERRORLEVEL 2 CALL:resta %numeros%
    IF ERRORLEVEL 1 CALL:suma %numeros%
```

## Enlaces de interés

[Tutorial Batch](https://todohacker.com/tutoriales/lenguaje-batch) <br>
[CMD - Batch](http://batchcoder.blogspot.com/2014/12/) <br>

## Imágen programa

![Ley Ohm](./nuevaCarpeta/progbatleyohm.png)
