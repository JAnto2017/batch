@ECHO OFF

REM Programa para conocer si hay conexión a Internet.
REM Comprueba cada 30 segundos si existe conexión a Internet.
REM Se hace ping a IP inexistente y se repite el proceso.
REM ---------------------------------------------------------------

color 0E
::mode con cols=70 lines=8

:START
set CONNECT=SI

ping 72.14.204.147 | find "TTL=" > nul

if not ERRORLEVEL 1 goto SI
if ERRORLEVEL 1 set CONNECT=NO

echo %CONNECT% tienes conexión a Internet.
pause
ping 1.1.1.1 -n 10 -w 3000 > nul
cls
goto START
pause>nul
exit

:SI
echo Estas conectado a Internet.
ping 1.1.1.1 -n 1 -w 3000 > nul
::cls
goto START
pause>nul
