@ECHO OFF
REM Programa que permite calcular con la Ley de Ohm
REM -----------------------------------------------

setlocal enabledelayedexpansion
echo Calculadora de la Ley de Ohm

:Menu
cls
echo 1.Calcular Voltaje:
echo 2.Calcular Corriente:
echo 3.Calcular Resistencia:
set /p opcion="Seleccionar una opcionn 1/2/3: "

::---------------------------------------------------

if %opcion%==1 (goto calcV) 
if %opcion%==2 (goto calcA) 
if %opcion%==3 (goto calcR) 
if %opcion% GTR 3 echo Error
goto Menu

::---------------------------------------------------

:calcV
set /p corriente="Intro corriente (A): "
set /p resistencia="Intro resistencia (R): "
set /a voltaje=%corriente% * %resistencia%
echo "El voltaje v=I*R: %voltaje% v.
exit /b

::---------------------------------------------------

:calcA
set/p voltios="Intro voltaje (V): "
set/p resist="Intro resistencia (Ohm): "
set/a volFloat=!voltios!*10000
set/a resultado=!volFloat! / !resist!
set resultado=!resultado:~0,-4!.!resultado:~-4!

echo La corriente I=V/R: %resultado% A.
exit/b

::----------------------------------------------------

:calcR
set/p voltaj="Intro voltaje (V): "
set/p corrient="Intro corriente (A): "
set/a voltajFloat=!voltaj!*10000
set/a resisResul=!voltajFloat! / !corrient!
set resisResul=!resisResul:~0,-4!.!resisResul:~-4!

echo La resistencia R=V/I: %resisResul% ohmios.
exit/b
