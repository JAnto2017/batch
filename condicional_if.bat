@ECHO OFF

REM Ejemplo de uso del condicional IF
REM ------------------------------------

:inicio

set/p num="Intro la suma de 2+2= "
cls
echo

if %num%==4 (
	goto cuatro
) else ( 
	goto otro)

:cuatro
echo.
echo Correcto!!
echo.
pause>nul
exit

:otro
echo.
echo Mal!!
echo.
echo Vuelve a intentarlo
echo.
pause
cls
goto inicio
